;; -*- lisp -*-

(in-package :it.bese.rtf)

;;;; ** RTF-in-Lisp

;;;; Convenience functions for generating rtf code from an rtf sexp.

(defparameter *rtf-stream* nil
  "This variable is bound to the stream where rtf text is sent.")

(defmacro with-output-to-rtf ((filename
                               &key
                               (if-exists :error)
                               (if-does-not-exist :error))
                              &body body)
  (with-unique-names (stream)
    `(with-output-to-file (,stream ,filename
                                   :if-exists ,if-exists
                                   :if-does-not-exist ,if-does-not-exist)
       (with-rtf-stream (,stream)
         (funcall #'rtf ,@body)))))

(defmacro with-rtf-stream ((stream) &body body)
  `(let ((*rtf-stream* ,stream))
     ,@body))

(defun rtf (rtf)
  "Outputs, to *rtf-stream*, the rtf code corresponding to the
rtf-sexp RTF.

Each element of RTF can be a cons or a string (refered to as an
rtf-form below). If it is a string it is printed to the
*rtf-stream* directly (though rtf special characters will be
escaped). If it is a cons it must be of the form:

  (OPERATOR &optional PARAMETER)

\(unless OPERATOR is a keyword, and therefore a special rtf form)
Operator is assumed to name an rtf command. OPERATOR's
symbol-name will be downcase'd and printed to the
*rtf-stream* (this implementation of rtf produces only lowercase
rtf commands). PARAMETER, if present, must be a number and will
be outputed, as a base-10 decimal, to the *rtf-stream* directly
after the command. If PARAMETER is a floating point number it
will be printed to 3 decimal places. After PARAMETER, or after
the command if no parameter is present, a single space character
will be emitted.

Certain keywords are allowed in place of :OPERATOR, these are the
so-called 'rtf special operators'. The following special
operators are available:

 (:GROUP &body body) - prints body wrapped in #\{ and
 #\}. Example:

   (:group (pard \"bar\" par)) ==> {\pard bar\par}

 (:LIST &rest lists) - Each element of LISTS (which must be a
 list of rtf forms) is printed followed by a #\;. Example:

   (:list ((one) (two)) ((three))) ==>

   \one\two;\three;

 (:BIN byte-array) - Prints the length of BYTE-ARRAY and then the
 raw data in byte-array. This exists soley for the sake of
 embedding images in rtf documents.

 (:RAW string) - write STRING to *rtf-stream*, no kind of
 processing nor escaping is done.

In the case this documentation is out of sync regarding the code
see the methods defined on the generic function
handle-special-operator."
  (etypecase rtf
    (string (escape-to-rtf rtf))
    (cons
     (destructuring-bind (operator &rest args)
         rtf
       (cond
         ((keywordp operator)
          (handle-special-operator operator args))
         ((symbolp operator)
          (write-string "\\" *rtf-stream*)
          (write-string (string-downcase (symbol-name operator)) *rtf-stream*)
          (cond
            ((null args)
             ;; no parameters, do nothing
             nil)
            ((and (= 1 (length args))
                  (numberp (first args)))
             ;; numeric parameter
             (let ((*print-base* 10))
               (etypecase (first args)
                 (integer (princ (first args) *rtf-stream*))
                 (float (format *rtf-stream* "~,3F" (first args)))
                 (rational
                  (format *rtf-stream* "~,3F" (float (first args)))))))
            (t
             ;; huh?
             (error "Badly formatted RTF. The parameter to ~S must be a single number, not ~S." operator args)))
          (write-char #\Space *rtf-stream*))
         (t (error "Badly formatted RTF: ~S" rtf)))))))

(defun escape-to-rtf (string)
  (loop
     for char across string
     do (case char
          (#\{ (write-string "\\{" *rtf-stream*))
          (#\} (write-string "\\}" *rtf-stream*))
          (#\\ (write-string "\\\\" *rtf-stream*))
          (t (write-char char *rtf-stream*)))))

(defgeneric handle-special-operator (operator args))

(defmethod handle-special-operator ((operator (eql :group)) args)
  (write-string "{" *rtf-stream*)
  (dolist (arg args)
    (rtf arg))
  (write-string "}" *rtf-stream*))

(defmethod handle-special-operator ((operator (eql :list)) args)
  (dolist (arg args)
    (dolist (sub arg)
      (rtf sub))
    (write-string ";" *rtf-stream*)))   ;

(defmethod handle-special-operator ((operator (eql :bin)) args)
  (let ((data (first args)))
    (write-string "\\bin" *rtf-stream*)
    (princ (length data) *rtf-stream*)
    (write-char #\Space *rtf-stream*)
    (write-string (arnesi:octets-to-string data :latin-1)
                  *rtf-stream*)))

(defmethod handle-special-operator ((operator (eql :sdata)) args)
  (let ((data (first args)))
    (macrolet ((%write-byte (byte)
                 `(ecase ,byte
                    ,@(loop
                         for i from 0 upto #xff
                         collect (list i `(write-string ,(format nil "~2,'0X" i) *rtf-stream*))))))
        (loop
           for byte across data
           do (%write-byte byte)))
    (write-char #\Space *rtf-stream*)))

(defmethod handle-special-operator ((operator (eql :raw)) args)
  (dolist (a args)
    (write-string a *rtf-stream*)))
