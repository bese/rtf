;; -*- lisp -*-

(in-package :it.bese.rtf)

(defun fonttbl (&rest fonts)
  "Creates a fonttbl group.

FONTS must be a list of strings naming the fonts to use. FONTS is
processed in order, so the first element of FONTS will be
accesable as (f 0), the second as (f 1) and so on."
  `(:group (fonttbl)
           ,@(loop
                for i upfrom 0
                for font in fonts
                collect `(:group (f ,i) ,font))))

(defun colortbl (&rest colors)
  "Creates a colortbl group.

Each element of COLORS must be list of three elements (red green
blue). Each element must be a (integer 0 255) and will be passed
directly to the corresponding \\red, \\green or \\blue command."
  `(:group (colortbl)
           (:list ,@(loop
                       for color-spec in colors
                       if (null color-spec)
                         collect nil
                       else
                         collect (destructuring-bind (red green blue)
                                     color-spec
                                   `((red ,red) (green ,green) (blue ,blue)))))))

(defun pard (&rest body)
  "Generate a paragraph group."
  (append
   (list :group '(pard))
   body
   (list '(par))))

(defun table (options &rest rows)
  (destructuring-bind (&key (cell-width 1000)
                            (left-edge-offset nil)
                            (cell-padding nil)
                            (cell-padding-left cell-padding)
                            (cell-padding-right cell-padding)
                            (cell-padding-top cell-padding)
                            (cell-padding-bottom cell-padding))
      options
    (let ((tbldef `((trowd)
                    (trbrdt 0)
                    (trbrdb 0)
                    (trbrdr 0)
                    (trbrdr 0)
                    (trautofill 1)
                    ,@ (etypecase left-edge-offset
                         (null nil)
                         (integer `((trleft ,left-edge-offset)))
                         ((eql :center) `((trqc)))
                         ((eql :left) `((trql)))
                         ((eql :right) `((trqr))))
                    ,@ (when cell-padding-left
                         `((trpaddfl 3)
                           (trpaddl ,cell-padding-left)))
                    ,@ (when cell-padding-right
                         `((trpaddfr 3)
                           (trpaddr ,cell-padding-right)))
                    ,@ (when cell-padding-top
                         `((trpaddft 3)
                           (trpaddt ,cell-padding-top)))
                    ,@ (when cell-padding-bottom
                         `((trpaddfb 3)
                           (trpaddb ,cell-padding-bottom))))))
      `(:group
        ,@(loop
             for row in rows
             append tbldef
             append (loop
                       with offset = 0
                       for cell in row
                       append `((clvertalc)
                                (clbrdrt 0)
                                (clbrdrb 0)
                                (clbrdrl 0)
                                (clbrdrr 0)
                                (clcbpat 17))
                       if (eql :options (first cell))
                         collect (destructuring-bind (&key cell-width)
                                    (second cell)
                                  (incf offset cell-width)
                                  `(cellx ,offset))
                         and do (setf cell (cddr cell))
                       else
                         do (incf offset cell-width) and
                         collect `(cellx ,offset)
                       append  `((intbl)
                                 ,@cell
                                 (cell)))
             collect `(row))))))

