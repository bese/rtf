;; -*- lisp -*-

(in-package :common-lisp-user)

(defpackage :it.bese.rtf
  (:use :common-lisp
        :arnesi)
  (:nicknames :rtf)
  (:export #:rtf
           #:*rtf-stream*
           #:with-rtf-stream
           #:with-output-to-rtf

           #:fonttbl
           #:table
           #:colortbl
           #:pard))

;;;; * Introduction

;;;; rtf is a common lisp library for creating Rich Text Format
;;;; files. RTF provides a simple sexp-based syntax for RTF data and a
;;;; collection of convenience functions for creating documents.

;;;; ** Usefull rtf docs on the net

;;;; http://search.cpan.org/~sburke/RTF-Writer/lib/RTF/Cookbook.pod


