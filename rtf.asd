;; -*- lisp -*-

(defpackage :it.bese.rtf.system
  (:documentation "ASDF System package for rtf.")
  (:use :common-lisp :asdf))

(in-package :it.bese.rtf.system)

(defsystem :rtf
  :components ((:static-file "rtf.asd")
               (:module :src
                :components ((:file "packages")
                             (:file "rtf" :depends-on ("syntax" "packages"))
                             (:file "syntax" :depends-on ("packages")))))
  :depends-on (:arnesi))

;;;;@include "src/packages.lisp"
